﻿using ItAcademy.Control.AboutPerson.ViewModel;
using ItAcademy.Control.AboutPerson.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItAcademy.Control.AboutPerson.BusinessLogic
{
    public class UserService : IUserService
    {
        IContext _context;// = new Context();
        public UserService(IContext context)
        {
            _context = context;
        }
        public IEnumerable<UserInfo> GetUsers()
        {
            var usersResult = new List<UserInfo>();

            var users = _context.GetAll();
            foreach (var u in users)
            {
                usersResult.Add(new UserInfo { Name = u.Name, Surname = u.Surname });
            }
            return usersResult;
        }
        public UserInfo GetUser(int? id)
        {
            throw new NotImplementedException();
        }
    }
}




//public List<UserInfo> GetUser()
//{
//    var users = new List<UserInfo>();

//    using (var db = new InfoDataBaseEntities())
//    {
//        var person = from whoAmI in db.Users
//                     select whoAmI;
//        foreach (var p in person)
//        {
//            users.Add(new UserInfo
//            {
//                Name = p.Name,
//                Surname = p.Surname
//            });
//        }
//    }

//    return users;
//}
