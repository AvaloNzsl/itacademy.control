﻿using ItAcademy.Control.AboutPerson.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItAcademy.Control.AboutPerson.BusinessLogic
{
    public interface IUserService
    {
        UserInfo GetUser(int? id);
        IEnumerable<UserInfo> GetUsers();
    }
}
