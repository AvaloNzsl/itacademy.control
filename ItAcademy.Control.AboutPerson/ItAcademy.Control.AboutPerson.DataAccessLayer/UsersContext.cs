﻿using ItAcademy.Control.AboutPerson.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItAcademy.Control.AboutPerson.DataAccessLayer
{
    public class UsersContext : DbContext
    {
        public DbSet<Users> UsInfo { get; set; }
        public DbSet<Details> UsDetails { get; set; }
    }
}
