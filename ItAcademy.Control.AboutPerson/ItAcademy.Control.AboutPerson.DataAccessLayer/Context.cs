﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItAcademy.Control.AboutPerson.DataAccessLayer
{
    public class Context : IContext
    {
        public IEnumerable<Users> GetAll()
        {
            InfoDataBaseEntities db = new InfoDataBaseEntities();
            return db.Users;
        }
    }
}
