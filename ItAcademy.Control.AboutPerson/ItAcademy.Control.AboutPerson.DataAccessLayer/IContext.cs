﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItAcademy.Control.AboutPerson.DataAccessLayer
{
    public interface IContext
    {
        IEnumerable<Users> GetAll();
    }
}
