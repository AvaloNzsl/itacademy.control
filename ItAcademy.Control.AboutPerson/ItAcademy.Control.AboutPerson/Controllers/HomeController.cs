﻿using ItAcademy.Control.AboutPerson.BusinessLogic;
using ItAcademy.Control.AboutPerson.DataAccessLayer;
using ItAcademy.Control.AboutPerson.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ItAcademy.Control.AboutPerson.Controllers
{
    public class HomeController : Controller
    {
        IUserService _userService;// = new PhotoService(); 

        public HomeController(IUserService userService)
        {
            _userService = userService;
        }
        // GET: Home
        public ActionResult InfoPage()
        {
            return View(_userService.GetUsers());
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Users ui)
        {
            using (var db = new InfoDataBaseEntities())
            {
                db.Users.Add(ui);
                db.SaveChanges();
            }
            return RedirectToAction("InfoPage");
        }
        InfoDataBaseEntities db = new InfoDataBaseEntities();
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var b = db.Users.Find(id);
            if (b == null)
            {
                return HttpNotFound();
            }
            return View(b);
        }
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int? id)//, Users us)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var b = db.Users.Find(id);
            if (b == null)
            {
                return HttpNotFound();
            }
            int? idD = b.DetailsId;
            if (idD == b.DetailsId)
            {
                var d = db.Details.Find(idD);
                db.Details.Remove(d);                
            }
            db.Users.Remove(b);
            db.SaveChanges();
            return RedirectToAction("InfoPage");
        }
    }
}