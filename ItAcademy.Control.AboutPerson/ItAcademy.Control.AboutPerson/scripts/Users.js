﻿$(document).ready(function () {
    $("#loadUsers").click(function () {
        Users.getUsers();
    });
});

Users = {
    list: [],
    getUsers: function () {
        $.ajax({
            type: "GET",
            url: "/api/users",  
            asynch: true,
            success: function (output, status, xhr) {
                Users.list = output;
                Users.renderList();
            },
            error: function () {
                alert("Error occured");
            }
        });
    },
    renderList: function () {
        $('#users').empty();
        $('#usersTemplate').tmpl(Users.list).appendTo('#users');
    }
}

