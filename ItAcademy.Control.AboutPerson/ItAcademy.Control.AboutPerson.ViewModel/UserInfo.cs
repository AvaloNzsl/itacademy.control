﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItAcademy.Control.AboutPerson.ViewModel
{
    public class UserInfo
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int? DetailsId { get; set; }
    }
    public class UserDetails
    {
        public int ID { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
    }
}
