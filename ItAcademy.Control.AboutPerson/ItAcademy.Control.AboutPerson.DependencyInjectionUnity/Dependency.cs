﻿using ItAcademy.Control.AboutPerson.BusinessLogic;
using ItAcademy.Control.AboutPerson.DataAccessLayer;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ItAcademy.Control.AboutPerson.DependencyInjectionUnity
{
    public class Dependency : IDependencyResolver
    {
        readonly IUnityContainer container;

        public Dependency()
        {
            container = new UnityContainer();
            container.RegisterType<IContext, Context>();
            container.RegisterType<IUserService, UserService>();
        }

        public object GetService(Type serviceType)
        {
            try
            {
                return container.Resolve(serviceType);
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return container.ResolveAll(serviceType);
            }
            catch
            {
                return null;
            }
        }
    }
}
